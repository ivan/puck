
class AbstractPlugin:
    def languages(self):
        return [];

    def name(self):
        return "";

    def process(self, source, annotations):
        return source;

__plugins = {}

def plugins():
    return __plugins

def plugin(language):
    if not language in __plugins:
        print("ERROR: Unknown language: ", language)
        return None

    return __plugins[language]

def register(plugin):
    for language in plugin.languages():
        __plugins[language] = plugin

