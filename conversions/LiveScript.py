#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

from subprocess import Popen, PIPE, STDOUT

from . import Plugins

import Indent

class LiveScriptPlugin(Plugins.AbstractPlugin):
    def languages(self):
        return ['live', 'livescript', 'ls'];

    def name(self):
        return "LiveScript";

    def process(self, source, annotations):
        if type(source) is list:
            source = '\n'.join(source)

        live_compiler = Popen(['lsc', '-b', '-c', '--no-header', '-s'],
                                stdout=PIPE, stdin=PIPE, stderr=STDOUT)

        output = live_compiler.communicate(bytes(source, 'UTF-8'))[0]

        status = live_compiler.wait()

        if status == 0:
            return (True,
                "{\n" +
                Indent.indent_block(output.decode('UTF-8').rstrip(), level=1) +
                "\n}")
        else:
            return (False, output.decode('UTF-8'))


Plugins.register(LiveScriptPlugin())
