#!/usr/bin/python3

#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import yaml
import sys
import os

import Conversions, Indent, Writer

from modules import *

if (len(sys.argv) < 2):
    raise Exception("Invalid invocation", "Usage: puck filename.yqml")

source_file = open(sys.argv[1], 'r')
source_raw  = source_file.read()

data = yaml.load(source_raw)

print(data)


##
# This method parses the import statements from the document,
# and extracts the main object definition

def write_root(root):
    imports = root['imports']
    ImportDeclarations.write_imports(imports)
    del root['imports']

    Writer.write_object(root)


# Write the output

header_file = open(os.path.dirname(os.path.realpath(__file__)) + "/template/header", 'r')
print(header_file.read())

write_root(data)

