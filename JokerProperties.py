#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import Indent, Writer

separator_char = ','
invert_char = '^'
joker_char = '_'

class Pattern:
    prefix = ""
    main = []
    suffix = ""

    def __init__(self, line):
        parts = line.split('|')

        if len(parts) == 1:
            self.prefix = ""
            self.main   = parts[0].split(separator_char)
            self.suffix = ""

        elif len(parts) == 3:
            self.prefix = parts[0]
            self.main   = parts[1].split(separator_char)
            self.suffix = parts[2]

        elif len(parts) == 2:
            if separator_char in parts[0] or invert_char in parts[0]:
                self.prefix = ""
                self.main   = parts[0].split(separator_char)
                self.suffix = parts[1]

            elif separator_char in parts[1] or invert_char in parts[1]:
                self.prefix = parts[0]
                self.main   = parts[1].split(separator_char)
                self.suffix = ""

            else:
                raise Exception("Missing pattern", str(parts))

        if len(self.main) == 1 and self.main[0][0] == '^':
            what = self.main[0][1:]
            self.main = [key for key in ["top", "left", "right", "bottom"] if key != what]




def is_joker(key):
    return "^top" in key or \
           "^left" in key or \
           "^right" in key or \
           "^bottom" in key or \
           separator_char in key


def write_joker_properties(key, val):

    pattern = Pattern(key)

    for key in pattern.main:
        Indent.write(pattern.prefix + key + pattern.suffix + ": ")

        write_replaced_value(key, str(val))


def write_replaced_value(key, val):
    joker_position = val.find(joker_char)

    if joker_position == -1:
        Writer.writeln_value(val)

    elif joker_position == 0 or val[joker_position - 1] == '.':
        Writer.writeln_value(val.replace(
            joker_char, key[0].lower() + key[1:]))

    else:
        Writer.writeln_value(val.replace(
            joker_char, key[0].upper() + key[1:]))

