#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


##
# Printing the indented text

output_indent = 0

def spaces():
    global output_indent
    return " " * output_indent * 4

def write(text):
    global output_indent
    print(spaces() + text, end="")

def write_block(text, indent_first_line=True):
    global output_indent
    print(indent_block(text, -1, indent_first_line))

def indent_block(text, level = -1, indent_first_line=True):
    line_indent = spaces() if level == -1 else " " * output_indent * 4
    prefix = line_indent if indent_first_line else ""

    return prefix + text.replace('\n', '\n' + line_indent)

def increase():
    global output_indent
    output_indent = output_indent + 1

def decrease():
    global output_indent
    output_indent = output_indent - 1
