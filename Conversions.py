#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import sys

import Indent
from conversions import *
from conversions import Plugins

##
# Processing the string

def __print_source(source_lines):
    for index, line in enumerate(source_lines):
        print(repr(index + 1).rjust(4), line, file=sys.stderr)


def __extract_annotations(source_lines):
    annotations = {}

    first_line = source_lines[0]

    if first_line[0] == '@':
        # This is an annotation line
        annotations_definition = first_line[1:]
        annotations_definition = annotations_definition.split(';')

        for annotation in annotations_definition:
            annotation = annotation.split('=', 1)
            if len(annotation) == 1:
                annotations["lang"] = annotation[0]
            else:
                annotations[annotation[0]] = annotation[1]

        source_lines = source_lines[1:]

    return (source_lines, annotations)


def process(source):
    (source_lines, annotations) = __extract_annotations(source.split('\n'))

    language = annotations.get("lang")

    if language:
        plugin = Plugins.plugin(language)

        if plugin:
            (success, result) = plugin.process(source_lines, annotations)

            if not success:
                print("Error: Compilation failed", file=sys.stderr)
                print("Source:", file=sys.stderr)
                __print_source(source_lines)
                print("Error message:\n", result, file=sys.stderr)

            return (success, result)

        else:
            return (False, source)

    return (True, source)



