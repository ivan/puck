#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import Indent, Conversions, CommonFormats, Writer, JokerProperties

def endl():
    write("\n")

def write(what):
    print(what, end="")

def writeln(what):
    write(what)
    endl()

##
# Print out the value. This works for lists, strings
# and other non-dict type values

def write_value(value):
    if type(value) is list:
        # Printing the list as [ item, item, item ]

        writeln("[")
        Indent.increase()

        first = True

        for list_item in value:
            if not first:
                Indent.write(",\n")
            first = False
            write(Indent.spaces())
            write_object(list_item)

        Indent.decrease()
        Indent.write("]")


    elif type(value) is str:
        if "\n" in value:
            # If this is a multi-line string, we need to process it
            (success, value) = Conversions.process(value)

            if success:
                Indent.write_block(value, indent_first_line=False)
            else:
                Indent.write_block("/*\n " + value + " \n*/", indent_first_line=False)


        elif value[0] == '^':
            # If you want to force something to be a string, ...
            write('"' + value[1:] + '"')

        else:
            write(value)

    else:
        write(str(value))

def writeln_value(value):
    write_value(value)
    endl()

##
# Print the object body

def write_object_body(obj, identifier):
    writeln(" {")
    Indent.increase()

    if identifier:
        Indent.write("id: " + identifier)
        Writer.endl()

    for key in obj:
        val = obj[key]

        if key == "signals":
            write_signals(val)

        elif key == "functions":
            write_functions(val)

        elif key == "properties":
            write_properties(val)

        elif type(val) is dict:
            # So, this object is an object :)
            # If the key ends in a dot, it means
            # it is supposed to be a shorthand,
            # Not a real object

            if key[-1] == '.':
                Indent.write(key[:-1])
                write_object_body(val, None)

            elif key == "anchors" or key == "font":
                Indent.write(key)
                write_object_body(val, None)

            else:
                Indent.write(key + ": ")
                write_object(val)

        elif JokerProperties.is_joker(key):
            JokerProperties.write_joker_properties(key, val)

        else:
            Indent.write(key + ": VAL ")
            writeln_value(val)


    Indent.decrease()
    Indent.write("}")
    Writer.endl()


##
# Prints the whole object

def write_object(obj):
    if len(obj) == 1:
        # If this is an object that has only one value inside,
        # the key is type, name or something similar
        for item in obj:
            (what, alias) = CommonFormats.split_alias_definition(item)

            write(what)
            write_object_body(obj[item], alias)

    else:
        # If there are multiple keys and values,
        # pretend it is the same as object insides
        write_object_body(obj, None)


##
# Generic function that prints out lists as separate
# items instead of actually printing out an actual list

def write_item_list(data, item_prefix):
    def write_single_item(item):
        Indent.write(item_prefix + " " + str(item))

    if type(data) is list:
        for item in data:
            write_single_item(item)
    else:
        write_single_item(data)

    endl()



##
# Printing out the QML signal definitions

def write_signals(data):
    write_item_list(data, "signal")


##
# Printing out the function definitions

def write_functions(data):
    for function in data:
        function_signature = function
        if not '(' in function:
            function_signature = function + "()"

        Indent.write("function " + function_signature + " ")
        writeln_value(data[function])


##
# Printing out the QML property definitions

def write_properties(data):
    if type(data) is list:
        for item in data:
            for sub in item:
                Indent.write("property " + sub + ": ")
                writeln_value(item[sub])

    else:
        for item in data:
            Indent.write("property " + item + ": ")
            writeln_value(item)

    endl()

