#  Copyright (C) 2015 Ivan Cukic <ivan.cukic(at)kde.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 2,
#  or (at your option) any later version, as published by the Free
#  Software Foundation
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the
#  Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import Indent, Writer, CommonFormats

##
# Printing out the QML imports

def write_imports(decls):
    for decl in decls:
        write_import_statement(decl)
    Writer.endl()

def write_aliased_import_statement(import_library, alias):
    Indent.write("import ")
    Writer.write_value(import_library)
    Writer.write(" as ")
    Writer.write_value(alias)
    Writer.endl()

def write_import_statement(decl):
    (what, alias) = CommonFormats.split_alias_definition(decl)

    if alias:
        write_aliased_import_statement(what, alias)

    else:
        Indent.write("import ")
        Writer.writeln_value(what)

